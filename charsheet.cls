\NeedsTeXFormat{LaTeX2e}[1994/06/01]
\ProvidesClass{charsheet}
    [2016/11/09 v0.1 Charsheet class (currently) for Shadowrun 5]

\def\@language{english}
\DeclareOption{german}{\def\@language{german}}
\DeclareOption{ngerman}{\def\@language{ngerman}}
\DeclareOption{english}{\def\@language{english}}

\ProcessOptions\relax

\LoadClass[a4paper]{article}

\RequirePackage[\@language]{babel}
\RequirePackage[\@language]{translator}
\usedictionary{shadowrun}

\newcommand{\@translateUC}{}
\newcommand{\translateUC}[1]{\translatelet\@translateUC{#1}\MakeUppercase{\@translateUC}}

\RequirePackage{ifthen}

\RequirePackage[%
    margin=0mm,
    centering,
]{geometry}
\setlength{\parindent}{0pt}
\pagestyle{empty}
\RequirePackage{hyperref}
\hypersetup{%
    pdfborder = {0 0 0}
}

\RequirePackage{xcolor}
\definecolor{border}{rgb}{0.494117647059,0.0588235294118,0.0705882352941}
\definecolor{line}{RGB}{109,110,112}
\definecolor{font}{RGB}{39,36,37}
\definecolor{monitor}{RGB}{176,178,180}

\RequirePackage{fontspec}
\setmainfont[
    Ligatures=TeX,
    UprightFont={* Condensed Bold},
    ItalicFont={* Condensed Light Italic},
    BoldFont={* Condensed Bold},
%    BoldItalicFont={* Condensed Bold Oblique},
    ]{Open Sans}
\newfontfamily{\heading}
    [Ligatures=TeX]
    {Grishenko NBP}
\newfontfamily{\lightFont}[
    Ligatures=TeX,
    UprightFont={* Condensed Light},
    BoldFont={* Condensed Bold},
    ItalicFont={* Condensed Light Italic},
]{Open Sans}
\newfontfamily{\wide}[
    Ligatures=TeX,
    UprightFont={* Light},
    BoldFont={* Bold},
    ItalicFont={* Light Italic},
]{Open Sans}
\newcommand{\light}[1]{{\lightFont\color{line} #1}}

\RequirePackage{tikz}
\usetikzlibrary{backgrounds}
\usetikzlibrary{calc}
\usetikzlibrary{fadings}
\usetikzlibrary{positioning}
\usetikzlibrary{arrows.meta}
\tikzset{%
    border/.style={draw=border, line width=.3mm},
    border end/.style={border, path fading=south},
    write/.style={draw=line, line width=.3mm},
    monitor border/.style={draw=line, line width=.3mm, color=border},
    monitor line/.style={draw=line, line width=.5mm, color=monitor},
}

\newcommand{\setnewlength}[2]{\newlength{#1}\setlength{#1}{#2}}

\RequirePackage{calc}
\setnewlength{\BoxWidth}{95mm}
\setnewlength{\BoxSep}{3mm}
\newlength{\TitleWidth}

\setnewlength{\BoxInnerUpperMargin}{2mm}
\setnewlength{\BoxInnerBottomMargin}{2mm}
\setnewlength{\BoxTitleHeight}{4.5mm}
\setnewlength{\BoxInnerSideMargin}{4mm}
\setnewlength{\BoxInnerVertMargin}{2mm}
\setnewlength{\VerticalBoxDistance}{3mm}
\setnewlength{\ContentWidth}{\BoxWidth-\BoxInnerSideMargin*2}
\setnewlength{\LineDistance}{4.5mm}
\setnewlength{\LineGap}{3mm}

\newlength{\charheight}
\newlength{\chardepth}
\setlength{\charheight}{2mm}
\setlength{\chardepth}{0mm}
%\settoheight{\charheight}{M}
%\settodepth{\chardepth}{y}

\newenvironment{CharSheet}{%
    \color{font}
}{%
}

\newenvironment{CharSheetPage}{%
    \begin{tikzpicture}%
    \path
        (-.5*\textwidth,0mm) rectangle (.5*\textwidth,.\textheight);
    \node[anchor=north west] at (-.5*\textwidth,0mm)
    {\includegraphics[width=.6\textwidth]{Shadowrun-5-Schriftzug_1500x300px.png}};
    \node[anchor=north west,inner ysep=0pt] (notes) at (20mm,-18.8mm)
        {\heading \translateUC{notes}};
    \node[anchor=north west,inner ysep=0pt] (player) at (20mm,-14.8mm)
        {\heading \translateUC{player}};
    \node[anchor=north west,inner ysep=0pt] (charname) at (20mm,-10.8mm)
        {\heading \translateUC{character}};
    \coordinate (end) at (.5*\BoxSep+\BoxWidth,0mm);
    \foreach \n in {notes,charname,player}
        \draw[line width=.3mm] (\n.south east) -| (\n.south east -| end);
    \node[above]  at (0mm, 5mm-\textheight) {\footnotesize\color{line}
        \begin{tabular}{c} \translate{disclaimer} \end{tabular}};%

    \coordinate (NextboxLeft) at (-\BoxWidth-.5*\BoxSep,-30mm);
    \coordinate (NextboxRight) at (.5*\BoxSep,-30mm);
}{%
\end{tikzpicture}%
\newpage
}

\newcommand{\BoxTitle}[1]{\heading \color{white}
    {\def\translate##1{\translatelet\foo{##1}\MakeUppercase{\foo}}#1}}
\newcommand{\DrawBoxTitleRight}[2]{% node, title
    \settowidth\TitleWidth{\pgfinterruptpicture \BoxTitle{#2}\endpgfinterruptpicture}
    \addtolength\TitleWidth{2mm}
\path[border, fill=border] (#1)
    -- ++(0mm,1mm-\BoxTitleHeight) node[base right, xshift=2mm] {\BoxTitle{#2}}
    -- ++(0mm,-1mm) -- ++(\TitleWidth,0mm) -- ++(7.5mm,0mm)
    -- ++(-7.5mm,\BoxTitleHeight) -- cycle;
}
\newcommand{\DrawBoxTitleLeft}[2]{% node, title
    \settowidth\TitleWidth{\pgfinterruptpicture \BoxTitle{#2}\endpgfinterruptpicture}
    \addtolength\TitleWidth{2mm}
    \path[border, fill=border] (#1)
        -- ++(0mm,1mm-\BoxTitleHeight) node[base left,xshift=-2mm] {\BoxTitle{#2}}
        -- ++(0mm,-1mm) -- ++(-\TitleWidth,0mm) -- ++(-7.5mm,0mm)
        -- ++(7.5mm,\BoxTitleHeight) -- cycle;
}

\newcommand{\DrawBoxBorder}[2]{% uncut corner, opposite corner
    \coordinate (fadeuncut) at ($(#1 |- #2)!12mm!(#1)$);
    \coordinate (fadecut) at ($(#2)!12mm!(#1 -| #2)$);
    \coordinate (cornertop) at ($(#1 -| #2)!4mm!(#1)$);
    \coordinate (cornerbottom) at ($(#1 -| #2)!4mm!(#2)$);
    \path[border] (fadeuncut) -- (#1) -- (cornertop) -- (cornerbottom) -- (fadecut);
    %\path[border end] (fadecut) -- (#2);%
    \path[border end] (#1 |- #2) -- (fadeuncut);%
}

\newcommand{\alignongrid}[1]{
    \path
        let \p1 = (#1),
            \p2 = (\x1,{\LineDistance * floor(divide(\y1,\LineDistance))})
        in coordinate (#1) at (\p2);
    }

\renewenvironment{Box}[2]{%
    \alignongrid{Nextbox#1}
    \begin{scope}[
            shift={(Nextbox#1)},
            local bounding box=currentbox,
            execute at end scope={
                \coordinate (endbox)
                    at ($(currentbox.south west) - (0,\BoxInnerVertMargin)$);
                \alignongrid{endbox};
                \begin{pgfonlayer}{background}
                    \node[draw, rectangle] at (currentbox.center) {};
                    \ifthenelse{\equal{#1}{Left}}{
                        \coordinate (Up)
                            at ($(currentbox.north east) - (0,\BoxTitleHeight)$);
                        \coordinate (Bottom) at (endbox);
                        \DrawBoxTitleLeft{currentbox.north east}{#2}
                    }{
                        \coordinate (Up)
                            at ($(currentbox.north west) - (0,\BoxTitleHeight)$);
                        \coordinate (Bottom)
                            at (endbox -| currentbox.north east);
                        \DrawBoxTitleRight{currentbox.north west}{#2}
                    }
                    \DrawBoxBorder{Up}{Bottom}
                \end{pgfonlayer}
                \coordinate (Nextbox#1) at ($(endbox) - (0,\BoxSep)$);
            },
            inner sep=0pt,
            outer sep=0pt,
        ]
        \path (0,0) -- (\BoxWidth,0);
    \begin{scope}[
            shift={(\BoxInnerSideMargin,-\BoxInnerVertMargin-\BoxTitleHeight)},
        ]
    \coordinate (nextrow);
}{
    \end{scope}
    \end{scope}
}

\newenvironment{BoxLeft}[1]{%
    \begin{Box}{Left}{#1}
}{%
    \end{Box}
}

\newenvironment{BoxRight}[1]{%
    \begin{Box}{Right}{#1}
}{%
    \end{Box}
}

\newenvironment{BoxRow}{
    \alignongrid{nextrow}
    \begin{scope}[
            shift={(nextrow)},
            local bounding box=currentrow,
            every node/.style={
                anchor=north west,
                align=left,
                text height=\charheight,
                text depth=\chardepth,
            },
            execute at end scope={
                \coordinate (nextrow) at (currentrow.south west);
            },
        ]
}{
    \end{scope}
}

\newcommand{\Skip}[1]{
    \coordinate (nextrow) at ($(nextrow) + (0,-#1)$);
}

\newcommand{\SingleRow}[1]{\begin{BoxRow}#1\end{BoxRow}}

\newcommand{\Line}[2]{% x, width
    \path[write] (#1, -\LineDistance+.1mm) -- ++(#2,0mm);
}

\newcommand{\FullLine}{%
    \Line{0mm}{\ContentWidth}
}
\newcommand{\HalfLines}{%
    \Line{0mm}{.5*\ContentWidth-.5*\LineGap}
    \Line{.5*\ContentWidth+.5*\LineGap}{.5*\ContentWidth-.5*\LineGap}
}
\newcommand{\LabelAt}[3][right]{% x, label
    \node[base #1,inner sep=0pt] at (#2, -4mm)
        {\fontsize{7pt}{10.5pt}\selectfont #3};
}
\newcommand{\LabeledFullLine}[1]{%label
    \SingleRow{
        \FullLine
        \LabelAt{0mm}{#1}
    }
}
\newcommand{\EmptyFullLine}{%
    \SingleRow{\FullLine}
}

\setnewlength{\AttributeWidth}{15mm}
\newcommand{\Attribute}[2]{% x, attribute 
    \Line{#1}{\AttributeWidth}
    \Line{#1+\AttributeWidth+\LineGap}{.5*\ContentWidth-1.5*\LineGap-\AttributeWidth}
    \node[base left,inner sep=0pt] at (#1+\AttributeWidth, -4mm)
        {\fontsize{7pt}{10.5pt}\selectfont #2};
}
\newcommand{\AttributeLine}[2]{% left attribute, right attribute
    \SingleRow{%
        \Attribute{0mm}{#1}
        \Attribute{.5*\ContentWidth+.5*\LineGap}{#2}
    }
}

\newcommand{\ConditionMonitor}[4][]{% subcaption, x, rows, title
    \begin{scope}[xshift=#2,yshift=-8mm,scale=1.28]
        \node[above] at (1.5, 0) {#4};
        \draw[monitor line, fill] (.5,-.5) circle[radius=.07];
        \foreach \i in {2,...,#3} 
            \draw[monitor line] (.5,1.5-1*\i) -- ++(2,0) -- ++(-2,-1);
        \draw[monitor line, -{Stealth[]}] (.5,.5-1*#3) -- ++(2,0);
        \foreach \i in {0,...,3} \draw[monitor border] (\i,0) -- (\i,-#3);
        \foreach \i in {0,...,#3} \draw[monitor border] (0,-\i) -- (3,-\i);
        \foreach \i in {1,...,#3}
        \node[above left] at (3,-\i)
            {\fontsize{12pt}{18pt}\color{line}\selectfont -\i};
            \node[below] at (1.5,-#3)
            {\fontsize{7pt}{9pt}\light{\begin{tabular}{c}#1\end{tabular}}};
    \end{scope}
}

\newcommand{\HexMonitor}[1][12]{% x, fields
    \begin{BoxRow}[9mm]
        \begin{scope}[yshift=-4mm,scale=4.186]
            \draw[monitor border] (0,0)
                \foreach\i in {1,...,#1} { -- ++(30:1mm) -- ++(-30:1mm) --
                ++(0,-1mm) ++(0,1mm) };
            \draw[monitor border] (0,0) -- (0,-1mm)
                \foreach\i in {1,...,#1} { -- ++(-30:1mm) node[above=1.5mm] {\color{line}\i} -- ++(30:1mm) };
        \end{scope}
    \end{BoxRow}
}

\newcommand{\LargeHexMonitor}[1][12]{% x, fields
    \pgfmathsetmacro\firstlowbox{int(1 + #1)}
    \pgfmathsetmacro\lastlowbox{2* #1- 1}
    \begin{BoxRow}[18mm]
        \begin{scope}[yshift=-4mm,scale=4.186]
            \draw[monitor border] (0,0)
                \foreach\i in {1,...,#1} { -- ++(30:1mm) -- ++(-30:1mm) --
                ++(0,-1mm) ++(0,1mm) };
            \draw[monitor border] (0,0) -- (0,-1mm)
                \foreach\i in {1,...,#1} { -- ++(-30:1mm) node[above=1.5mm] {\color{line}\i} -- ++(30:1mm) };
            \draw[monitor border] (0,-1mm) ++(-30:1mm) -- ++(0,-1mm) 
            \foreach\i in {\firstlowbox,...,\lastlowbox} { -- ++(-30:1mm) node[above=1.5mm] {\color{line}\i} -- ++(30:1mm)
                -- ++(0,1mm) ++(0,-1mm)};
        \end{scope}
    \end{BoxRow}
}

\endinput

% vim: filetype=tex
